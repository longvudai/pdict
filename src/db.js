import firebase from 'react-native-firebase';
 let config = {
    apiKey: "AIzaSyCYxST030GDer5SOtH9eCnwDbSOXbOhsj4",
    authDomain: "dictionaryproject-f3605.firebaseapp.com",
    databaseURL: "https://dictionaryproject-f3605.firebaseio.com",
    projectId: "dictionaryproject-f3605",
    storageBucket: "dictionaryproject-f3605.appspot.com",
    messagingSenderId: "442924765406",

    // enable persistence by adding the below flag
    persistence: true,
  };
// let app = firebase.initializeApp(config);

const firebaseApp = firebase.app();
export default firebaseApp;
