import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, StatusBar, Alert } from 'react-native';

import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

const customCSS = `
    span.fi {
        color: #0061ff;
        font-weight: bold;
    }
    span.thn{
        color: #b51a00;
        font-style: italic;
    }
    span.bk {
        color: #006d8f;
        font-weight: bold;        
    }
    span.head {
        color: #E66C2C;
        font-family: "Times New Roman";
        font-weight: bold;
        font-size: 22px;
    } 
    span.maucau {
        color: green; 
        font-style: italic; 
        font-weight:bold;
    } 
    span.lt {
        color: #4f7a28;
        font-weight: bold
    }
    span.m {
        color: #9a244f;
        font-family: "Times New Roman";
        font-weight: bold;
    }
        `



export default class EditContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        title: this.props.navigation.state.params.title,
        content: this.props.navigation.state.params.content
    };
    this.firstLoaded = this.state
  }

  componentDidMount() {
      this.props.navigation.setParams({
            saveChanged: () => {
                this.getHTML()
            }
        })
  }
  
  _updateContent() {
    const {doc} = this.props.navigation.state.params

        console.log(this.state);

        doc.ref.update({
            title: `${this.state.title}`,
            content: `${this.state.content}`
        }).then(() => {
            console.log("Document successfully updated!");
            Alert.alert(
                'Saved!', 
                "Document successfully updated!", 
                [
                    {
                        text: 'OK', onPress:() =>
                            this.props.navigation.goBack()
                    }
                ],
                { cancelable: false }
            )
          }).catch(function(error) {
            console.error("Error updating document: ", error);
          });
  }

    render() {

        const {title, content, doc} = this.props.navigation.state.params
        

        return (
            // <View style={styles.container}>
            <View style={styles.container}>
            <StatusBar barStyle='light-content'/>
            <RichTextEditor
                ref={(r) => this.richtext = r}
                initialTitleHTML={title}
                initialContentHTML={`${content}`}
                customCSS={customCSS}
                editorInitializedCallback={() => this.onEditorInitialized()}
                />
                <RichTextToolbar
                    style={styles.toolBarContainer}
                	getEditor={() => this.richtext}
                />
                <Button
                    rounded = 'true'
                    title='Delete'
                    backgroundColor='red'
                    // style = {styles.button} 
                    onPress={() => {
                        doc.ref.delete()
                        .then(() => {
                            console.log("Document successfully deleted!");
                            Alert.alert(
                                'Success', 
                                "Document successfully deleted!", 
                                [
                                    {
                                        text: 'OK', onPress:() =>
                                            this.props.navigation.navigate('HomeScreen')
                                    }
                                ],
                                { cancelable: false }
                            )
                        })
                        .catch(function(error) {
                            console.error("Error removing document: ", error);
                            Alert.alert('Error removing document', error, {text: 'OK', onPress:() => {
                                
                            }})
                        });
                    }

                    }
                    />
                </View>
        );
      }

      onEditorInitialized() {
        this.setFocusHandlers();
      }
    
      async getHTML() {
        // const titleHtml = await this.richtext.getTitleHtml();
        var contentHtml = await this.richtext.getContentHtml();
        
        if (contentHtml.valueOf() == this.firstLoaded.content.valueOf()) {
            console.log('No Changed');
            return
        }
        
        // else Update
        // Replace ' with html Code
        contentHtml = contentHtml.replace(/'/gi, `&#39;`)

        this.setState({
            content: contentHtml
        })
        this._updateContent()
      }
    
      setFocusHandlers() {
        // this.richtext.setTitleFocusHandler(() => {
        //   alert('title focus');
        // });
        this.richtext.setContentFocusHandler(() => {
        //   alert('content focus');
        });
      }
  }
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#fff',
        flexDirection: "column"
    },
    toolBarContainer: {
        // flex: 1,
        // padding: 10,
        marginBottom: 10,
        height: 50
    }
  });