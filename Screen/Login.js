import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';

import LoginForm from '../Screen/LoginForm';
import firebase from 'react-native-firebase'
import firebaseApp from '../src/db';


const logo = require('../Resources/translate.png');

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      loading: true,
      authenticated: false,
    };
  }

  componentWillMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loading: false, authenticated: true });
      } else {
        this.setState({ loading: false, authenticated: false });
      }
    });
  }

  componentWillUnmount() {
    this.setState({ loading: true, authenticated: false });
  }


  render() {
    if (this.state.loading) return null; // Render loading/splash screen etc

    if (!this.state.authenticated) {
      return <Login />;
    }

    return <Home />;
  }
  
  render() {
    navigateScreen = (screen) => {
      const { navigate } = this.props.navigation
      navigate(screen)
    }

    const { navigate } = this.props.navigation

    // if (this.state.loading) return null; // Render loading/splash screen etc
    
    if (this.state.authenticated) {
      navigate('HomeScreen')
    }
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.logoContainer}>
          <Image
          style={styles.logo}
          source={logo}></Image>
          <Text style={styles.title}> Pdict </Text> 
        </View>

        <View style={styles.formContainer}>
            <LoginForm onPress={navigateScreen.bind(this)}></LoginForm>
        </View>

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3498db'
    },

    formContainer: {
      height: 240,
      // backgroundColor: '#FFF'
      
    },

    logoContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems:'center'
    },
    logo: {
      width: 100,
      height: 100
    },
    title: {
      textAlign: 'center',
      color: '#FFF',
      opacity: 0.8,
      fontWeight: '700'
    }
})
