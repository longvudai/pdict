# pDict

this is dictionary was created with react-native

## Installation
- Clone this repo in your machine
- Change to cloned directory
- Install dependencies:
 ``npm i``
or
``yarn install``
- then run app:
    - iOS

        ``react-native run-ios --simulator="iPhone SE"``

    - Android

        ``react-native run-android``

## Demo

Login and Register     |   Home Screen        | Search Words
:-------------------------:|:-------------------------:|:-------------------------:
![Alt Text](/demo/1.png "Screen 1")  |  ![Alt Text](/demo/2.png "Screen 2") | ![Alt Text](/demo/3.png "Screen 3")

Auto Complete Search            |  Editing  | Meaning
:-------------------------:|:-------------------------:|:-------------------------:
![Alt Text](/demo/4.png "Screen 4") | ![Alt Text](/demo/5.png "Screen 5") | ![Alt Text](/demo/6.png "Screen 6")